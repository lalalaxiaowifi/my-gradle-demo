plugins {
    //id("java")
    id("groovy")
}

repositories {
    google()
    mavenCentral()
    maven("../repo")
}

dependencies {
    implementation(gradleApi())
    implementation(localGroovy())
//    implementation(project(":bindview"))
    implementation("com.android.tools.build:gradle:4.2.2")
    annotationProcessor("com.google.auto.service:auto-service:1.0-rc4")
    implementation("com.google.auto.service:auto-service:1.0-rc4")
    implementation("com.squareup:javapoet:1.13.0")
}