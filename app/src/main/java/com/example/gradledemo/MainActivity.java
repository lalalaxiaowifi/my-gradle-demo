package com.example.gradledemo;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.luoye.bindview.ActivityBinding;
import com.luoye.bindview.BindView;
import android.os.Bundle;
import com.luoye.bindview.OnClick;

/**
 * 测试界面。
 */
@ActivityBinding
public class MainActivity extends AppCompatActivity {
    @BindView(id = R.id.tv1)
    TextView tv1;
    @BindView(id = R.id.tv2)
    TextView tv2;
    @BindView(id = R.id.tv3)
    TextView tv3;
    @BindView(id = R.id.tv4)
    TextView tv4;
    /*  @BindView(id=R.id.tv5)
      TextView tv5;*/
    @BindView(id = R.id.tv6)
    TextView tv6;
    @BindView(id = R.id.tv7)
    TextView tv7;
    @BindView(id = R.id.tv8)
    TextView tv8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @OnClick(ids = {R.id.tv1, R.id.tv2, R.id.tv3, R.id.tv4, R.id.tv5})
    public void onClick(View view) {
        Toast.makeText(this, "" + view.getId(), Toast.LENGTH_SHORT).show();
    }
}