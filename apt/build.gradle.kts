plugins {
    id("java")
}

dependencies {
    //implementation("com.luoye.bindview:bindview:1.0.0")
    implementation(project(":bindview"))
    // apt
    annotationProcessor("com.google.auto.service:auto-service:1.0-rc4")
    implementation("com.google.auto.service:auto-service:1.0-rc4")
    implementation("com.squareup:javapoet:1.13.0")
    implementation("com.google.code.gson:gson:2.10")
    // 黄油刀的源码 https://github.com/JakeWharton/butterknife
    //implementation("com.jakewharton:butterknife:10.2.3")
    //implementation("com.jakewharton:butterknife-compiler:10.2.3")
}