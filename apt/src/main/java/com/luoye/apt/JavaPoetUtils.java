package com.luoye.apt;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Modifier;
import java.io.IOException;

public class JavaPoetUtils {
    public static void createClass(Filer filer, String simpleName, String packageName) throws IOException {

        TypeSpec helloWorld = null;
        try {

            helloWorld = TypeSpec.classBuilder(simpleName+"JavaPoetBinding")
                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                    .superclass(Class.forName("com.luoye.bindview.AbBind"))
                    .addSuperinterface(IBinding.class)
                    .addMethod(getDebugMethod())
                    .addMethod(getDebugMethod1())
                    .addMethod(getIbinding())
                    .addMethod(getABbind())
                    .build();
            JavaFile.builder(packageName, helloWorld).build().writeTo(filer);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }

    private static MethodSpec getABbind() {
        return MethodSpec.methodBuilder("bind")
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .build();
    }

    private static MethodSpec getIbinding() {
        return MethodSpec.methodBuilder("onBinding")
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .build();
    }

    private static MethodSpec getDebugMethod1() {
        return MethodSpec.methodBuilder("bind")
                .addParameter(String.class,"name")
                .returns(void.class)
                .build();

    }

    private static MethodSpec getDebugMethod() {
        return MethodSpec.methodBuilder("debug").returns(void.class).build();
    }
}
