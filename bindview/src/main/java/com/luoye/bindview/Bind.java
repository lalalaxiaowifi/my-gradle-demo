package com.luoye.bindview;

public interface Bind<T> {
    void binding(T object);

    void bindView(T object);

    void bindClick(T object);
}
