plugins {
    id("java")
    //id("groovy")
}

repositories {
    google()
    mavenCentral()
    maven("../repo")
}

dependencies {
    implementation(gradleApi())
    implementation(localGroovy())
    implementation("org.javassist:javassist:3.20.0-GA")
    implementation("com.android.tools.build:gradle:4.2.2")
    implementation("com.luoye.bindview:bindview:1.0.0")
}