package luoye.java.plugin;

import com.android.build.gradle.BaseExtension;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class GradleJavaPlugin implements Plugin<Project> {
    @Override
    public void apply(Project target) {
        System.out.println("java Plugin  "+target.getName());
        target.getPlugins().forEach(new Consumer<Plugin>() {
            @Override
            public void accept(Plugin plugin) {
                System.out.println("plugin:"+plugin.getClass().getName());
            }
        });
        target.getExtensions().getExtraProperties().getProperties().forEach(new BiConsumer<String, Object>() {
            @Override
            public void accept(String s, Object o) {
                System.out.println("Extension:"+s);
            }
        });
        BaseExtension extension = target.getExtensions().findByType(BaseExtension.class);
        if (extension!=null){
            extension.registerTransform(new JavaSsistTransform(target));
        }else {
            System.out.println("extension是一个空对象呢");
        }


    }

}
