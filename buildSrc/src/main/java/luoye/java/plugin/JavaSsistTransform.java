package luoye.java.plugin;

import com.android.build.api.transform.Format;
import com.android.build.api.transform.JarInput;
import com.android.build.api.transform.QualifiedContent;
import com.android.build.api.transform.Transform;
import com.android.build.api.transform.TransformException;
import com.android.build.api.transform.TransformInvocation;
import com.android.build.gradle.BaseExtension;
import com.android.build.gradle.internal.pipeline.TransformManager;
import com.android.utils.FileUtils;

import org.gradle.api.Project;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.function.Consumer;

import javassist.ClassPool;
import javassist.NotFoundException;

/**
 * transform
 */
public class JavaSsistTransform extends Transform {
    Project target;
    ClassPool classPool = ClassPool.getDefault();
    public JavaSsistTransform(Project target) {
        this.target = target;
        BaseExtension extension = target.getExtensions().findByType(BaseExtension.class);
        extension.getBootClasspath().forEach(file -> {
            try {
                System.out.println("bootClasspath:"+file.getAbsolutePath());
                classPool.appendClassPath(file.getAbsolutePath());
            } catch (NotFoundException e) {
                e.printStackTrace();
            }
        });


    }

    @Override
    public String getName() {
        return "luoye";
    }

    @Override
    public Set<QualifiedContent.ContentType> getInputTypes() {
        return TransformManager.CONTENT_CLASS;
    }

    @Override
    public Set<? super QualifiedContent.Scope> getScopes() {
        return TransformManager.PROJECT_ONLY;
    }

    @Override
    public void transform(TransformInvocation transformInvocation) throws TransformException, InterruptedException, IOException {
        super.transform(transformInvocation);
        System.out.println("自定义transform 已经执行了呢");
        transformInvocation.getInputs().forEach(transformInput -> {
            // 处理自己的写的代码。
            transformInput.getDirectoryInputs().forEach(it -> {
                // it 就是我们拿到的class 文件哦。
                System.out.println("开始导入项目文件："+it.getFile().getAbsolutePath());
                File file = transformInvocation.getOutputProvider().getContentLocation(
                        it.getName(),
                        it.getContentTypes(),
                        it.getScopes(),
                        Format.DIRECTORY);

                try {
                    classPool.insertClassPath(it.getFile().getAbsolutePath());
                    new JavassistUtils(classPool,it.getFile());
                    FileUtils.copyDirectory(it.getFile(), file);
                } catch (Exception  e) {
                    throw new RuntimeException(e);
                }
            });
            // 处理jar 里面的代码。
            transformInput.getJarInputs().forEach(new Consumer<JarInput>() {
                @Override
                public void accept(JarInput it) {
                    File file = transformInvocation.getOutputProvider().getContentLocation(
                            it.getName(),
                            it.getContentTypes(),
                            it.getScopes(),
                            Format.JAR);
                    try {
                        FileUtils.copyFile(it.getFile(), file);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        });

    }

    @Override
    public boolean isIncremental() {
        return false;
    }
}
